<?php
require '../pages/functionPHP.php';
 ?>
<!DOCTYPE html>
<html lang="fr">
  <head>
    <style media="screen">
      .container1
      {
        margin-top: 150px;
        font-size: 1.5em;
      }
    </style>
    <meta charset="utf-8">
    <title>Page en construction</title>
    <script src="https://code.jquery.com/jquery-3.2.1.js"></script>
    <!--CSS_Bootstrap-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <!--Javascript_Bootstrap-->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="../assets/styles/main.css" />

    <!--Font -->
    <link href="https://fonts.googleapis.com/css?family=Holtwood+One+SC" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../css/style.css">

  </head>
  <body>
    <div class="navbar">
      <?php
         Navbar();
       ?>
    </div>

    <div class="container1">
          <p align="center">Page en construction...</p>
    </div>
  </body>
</html>
